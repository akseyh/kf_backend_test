export function filterDevices(devices, outages) {
  const OUT_DATE = new Date('2022-01-01T00:00:00.000Z').getTime()

  return devices.map(device => {
    if (!device?.id) return null
    return outages.filter(outage => outage.id === device.id && new Date(outage.begin).getTime() >= OUT_DATE).map(item => ({
      id: device.id,
      name: device.name,
      begin: item.begin,
      end: item.end,
    }))
  }).flat().filter(item => item)
}