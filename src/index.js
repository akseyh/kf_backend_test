import API from './api.helper.js'
import { filterDevices } from './utils.js'

const SITE_ID = 'norwich-pear-tree'

  ; (async () => {
    try {
      const outages = await API.getOutages()
      const siteInfo = await API.getSiteInfo(SITE_ID)

      const data = filterDevices(siteInfo.devices, outages)
      const response = await API.postOutages(SITE_ID, data)

      console.log(response)
    } catch (err) {
      console.log(err)
    }
  })()
