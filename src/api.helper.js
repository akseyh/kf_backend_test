import axios from 'axios'
import pRetry, { AbortError } from 'p-retry'
import config from './config.js'

class API {
  constructor() {
    this.api = axios.create({
      baseURL: 'https://api.krakenflex.systems/interview-tests-mock-api/v1',
      withCredentials: true,
      headers: {
        "x-api-key": config.API_KEY,
      }
    })
  }

  getOutages = async () => {
    const fetch = async () => {
      try {
        const response = await this.api.get('/outages')

        return response.data
      } catch (err) {
        if (err.response.status === 500) throw new Error(err.response.data.message)

        throw new AbortError(new Error(err.response.data.message));
      }
    }

    return pRetry(fetch, { retries: 3 })
  }
  getSiteInfo = async (siteId) => {
    const fetch = async () => {
      try {
        const response = await this.api.get(`/site-info/${siteId}`)

        return response.data
      } catch (err) {
        if (err.response.status === 500) throw new Error(err.response.data.message)

        throw new AbortError(new Error(err.response.data.message));
      }
    }

    return await pRetry(fetch, { retries: 3 })
  }
  postOutages = async (siteId, outages = []) => {
    const fetch = async () => {
      try {
        const response = await this.api.post(`/site-outages/${siteId}`, outages)

        return response.statusText
      } catch (err) {
        if (err.response.status === 500) throw new Error(err.response.data.message)

        throw new AbortError(new Error(err.response.data.message));
      }
    }

    return await pRetry(fetch, { retries: 3 })
  }
}

export default new API()