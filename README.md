# KrakenFlex Back End Test

## Introduction
It is a NodeJS program that solves the problem. You need to have NodeJS installed on your machine to run it.

## How to run
**Environment Variables**
Create a file called .env in the root directory of the project and add the following variables:
```
API_KEY={{YOUR_API_KEY}}
```

**Instal dependencies**
```bash
npm install
```

**Run the program**
```bash
npm start
```

## How to test
**Run the test command**
```bash
npm test
```

## Folder structure
**\_\_tests\_\_**
All tests are in this folder. The test file name is the same as the file being tested.

**api.helper.js**
This file contains the logic to call the API. It is a helper file that is used by the main program.

**config.js**
This file contains the configuration for the program.

**index.js**
This file contains the main logic of the program.

**utils.js**
This file contains the tested utility functions used by the program.